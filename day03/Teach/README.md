# CSS3

## 2D转换

  > 平移转换

    属性: transform
    属性值:水平平移: translateX() 竖直平移: translateY()

  > 旋转转换

    属性: transform
    属性值: rotate()

  > 缩放转换

    属性: transform
    属性值: scale()

  > 2D转换问题: 如何让一个元素水平垂直居中(2D转换的知识)

## 3D转换

  > 平移转换

    属性: transform
    属性值: translateX() translateY() translateZ()

  > 旋转转换

    属性: transform
    属性值: rotateX() rotateY() rotateZ()

## 动画

  > 过渡

    属性: transition
    属性值:
      过渡属性 比如 width height background
      过渡时间 比如 
      过渡速度
      过渡延迟

  > 动画

    属性: animation
    属性值:
      动画名称       animation-name: 定义的动画的名字
      动画持续时间   animation-duration:  time
      动画速度       animaiton-timing-function: ease ease-in ease-in-out ease-out linear steps
      动画延迟时间   animation-delay: time
      动画次数       animation-iteration-count: 可以任何表示次数的数字 也可以是infinite(永远)
      动画方向       animation-direction: normal(正常) alternate(交替反向) reverse(反向) 
      动画结束时状态 animation-fill-mode: none  forwards(停留在最后的状态) backwards(回到最初的状态)
      动画播放状态   animation-play-state: running(运行)  paused(停止)

## flex布局

  > 设置伸缩容器

    属性: display
    属性值: flex

  > 设置主轴方向

    属性: flex-direction
    属性值: row | row-reverse | column | column-reverse;

  > 设置伸缩项目在主轴方向上的对齐方式

    属性: justify-content
    属性值: flex-start | flex-end | center | space-between | space-around;

  > 设置伸缩项目是否换行

    属性: flex-wrap
    属性值:  nowrap | wrap 

  > 设置换行后的伸缩项目的对齐方式

    属性: align-content
    属性值: flex-start | flex-end | center | space-between | space-around | stretch;

  > 设置不换行的伸缩项目的对齐方式

    属性: align-items
    属性值: flex-start | flex-end | center | stretch;

  > 设置伸缩项目所占伸缩容器的剩余空间的比例

    属性: flex
    属性值: 比例

  > 设置伸缩项目的在页面中的顺序

    属性: order
    属性值: 值越小越靠前 默认为0

  > 设置某一个伸缩项目的对齐方式

    属性: align-self
    属性值: flex-start | flex-end | center | stretch;