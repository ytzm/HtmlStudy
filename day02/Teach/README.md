# CSS中的常用属性

## background(背景)

  > 背景颜色

    属性: background-color
    属性值:
          用英文单词代表的颜色 red pink hotpink
          用十六进制代表的颜色 #ff0000 #000000
          用三原色代表的颜色   rgb(255,255,255) rgba(255,0,0,0.4) 0.4代表的是透明度 0-1

  > 背景图像

    属性: background-image
    属性值:
            url("")或url()

  > 背景重复

    属性: background-repeat
    属性:
          repeat   背景图像将向垂直和水平方向重复。这是默认
          repeat-x 只有水平位置会重复背景图像
          repeat-y 只有垂直位置会重复背景图像
          no-repeat background-image不会重复

  > 背景附件

    属性: background-attachment
    属性:
          scroll 背景图片随页面的其余部分滚动。
          fixed  背景图像是固定的

  > 背景位置

    属性: background-position

  > 背景尺寸

    属性: background-size

## position(定位)

  > 绝对定位

    属性: position
    属性值:
          absolute 相对于 static 定位以外的第一个父元素进行定位。元素的位置通过 "left", "top", "right" 以及 "bottom" 属性进行规定

  > 固定定位

    属性: position
    属性值:
          fixed 生成绝对定位的元素，相对于浏览器窗口进行定位。元素的位置通过 "left", "top", "right" 以及 "bottom" 属性进行规定。

  > 相对定位

    属性: position
    属性值:
          relative 生成相对定位的元素，相对于其正常位置进行定位。(标准流)

## float(浮动)

  > 左浮动

    属性: float
    属性值:
          left 元素向左浮动
  > 右浮动

    属性: float
    属性值:
          right 元素向左浮动

## boxModel(盒模型)

  > 宽高(包裹的买的东西)

    属性: width height
    属性值:
          具体的数字

  > 内边距(包裹里的买的东西的包装内衬的泡沫)

    属性: padding
    属性值:
          具体的数字

  > 边框(包裹外面的快递袋或者纸箱子)

    属性: border
    属性值:
          边框宽度 border-width
          边框样式 border-style
          边框颜色 border-color

  > 外边距(包裹和包裹的距离)

    属性: margin
    属性值:
          具体的数字